{-# LANGUAGE BangPatterns #-}
module Body(
	Vector(Vector), getX, getY, getZ,
	toTuple,
	fromTuple,
	distanceSquared,
	distance,
	Body(Body), position, velocity, mass,
	calcAcceleration,
	FloatEq(..),
	energy,
) where
import System.Random
import Control.Applicative ((<$>), (<*>))
import Control.DeepSeq (NFData(..))

data Vector = Vector {
	getX :: {-# UNPACK #-} !Double,
	getY :: {-# UNPACK #-} !Double,
	getZ :: {-# UNPACK #-} !Double
} deriving (Eq, Show)

data Body = Body {
	position :: {-# UNPACK #-} !Vector,
	velocity :: {-# UNPACK #-} !Vector,
	mass :: {-# UNPACK #-} !Double
} deriving (Eq, Show)

instance NFData Vector where
    rnf (Vector x y z) = rnf x `seq` rnf y `seq` rnf z
instance NFData Body where
    rnf (Body p v m) = rnf p `seq` rnf v `seq` rnf m

toTuple (Vector x y z) = (x, y, z)
fromTuple (x, y, z) = Vector x y z

mapPair f (x, y) = (f x, f y)

mapVector :: (Double -> Double) -> Vector -> Vector
mapVector f (Vector x y z) = Vector (f x) (f y) (f z)

distanceSquared :: Vector -> Double
distanceSquared (Vector x y z) = d
	where
		!d = x*x + y*y + z*z

distance :: Vector -> Double
distance = sqrt . distanceSquared


instance Num (Vector) where
	Vector x1 y1 z1 + Vector x2 y2 z2 = Vector (x1+x2) (y1+y2) (z1+z2)
	{-# INLINE (+) #-}
	Vector x1 y1 z1 - Vector x2 y2 z2 = Vector (x1-x2) (y1-y2) (z1-z2)
	{-# INLINE (-) #-}
	Vector x1 y1 z1 * Vector x2 y2 z2 = Vector (x1*x2) (y1*y2) (z1*z2)
	{-# INLINE (*) #-}
	abs = mapVector abs
	signum = mapVector signum
	fromInteger i = Vector v v v
		where
			v = fromInteger i

instance Fractional (Vector) where
	Vector x1 y1 z1 / Vector x2 y2 z2 = Vector (x1/x2) (y1/y2) (z1/z2)
	{-# INLINE (/) #-}
	fromRational i = Vector v v v
		where
			v = fromRational i

instance Random (Vector) where
	random gen = (Vector x y z, gen4)
		where
			(x, gen2) = random gen
			(y, gen3) = random gen2
			(z, gen4) = random gen3

	randomR range gen = (Vector x y z, gen4)
		where
			(x, gen2) = randomR (mapPair getX range) gen 
			(y, gen3) = randomR (mapPair getY range) gen2
			(z, gen4) = randomR (mapPair getZ range) gen3

instance Random Body where
	random gen = (Body pos vel (abs mass), gen4)
		where
			(pos, gen2) = random gen
			(vel, gen3) = random gen2
			(mass, gen4) = random gen3

	randomR range gen = (Body pos vel (abs m), gen4)
		where
			(pos, gen2) = randomR (mapPair position range) gen 
			(vel, gen3) = randomR (mapPair velocity range) gen2
			(m, gen4) = randomR (mapPair mass range) gen3

eps = 0.01
class FloatEq a where
	(~=) :: a -> a -> Bool
instance FloatEq a => FloatEq [a] where
	l ~= r = and $ Prelude.zipWith (~=) l r
instance FloatEq Double where
	l ~= r = abs(l - r) < eps

instance FloatEq (Vector) where
	(Vector x1 y1 z1) ~= (Vector x2 y2 z2) = x1 ~= x2 && y1 ~= y2 && z1 ~= z2

instance FloatEq Body where
	(Body lPos lVel lMass) ~= (Body rPos rVel rMass) = lPos ~= rPos && lVel ~= rVel && lMass ~= rMass


energy :: [Body] -> Double
energy bs = foldr (\b acc -> (mass b * (distanceSquared $ velocity b) / 2) + foldr (f b) acc bs) 0 bs
	where
		f (Body pos1 _ mass1) (Body pos2 _ mass2) acc = acc + e
			where
				e = potentialEnergy pos1 mass1 pos2 mass2 * mass1


epsilon = 0.001
{-
a = G * mj * (rj - ri) / (|r|^2 + epsilon)^(3/2)
G is ommited currently since the data  from the language shootout which I am currently using seems to have the constant baked in
-}
calcAcceleration :: Double -> Vector -> Vector -> Double -> Vector
calcAcceleration dt pos1 pos2 mass = delta * Vector magnitude magnitude magnitude
	where
		delta = pos2 - pos1
		squaredDistance = distanceSquared delta
		magnitude = dt * mass / ((squaredDistance * sqrt squaredDistance) + epsilon)

potentialEnergy :: Vector -> Double -> Vector -> Double -> Double
potentialEnergy p1 mass1 p2 mass2 = mass1 * mass2 / distance
	where
		distance = sqrt $ distanceSquared (p2 - p1)


