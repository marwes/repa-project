{-# LANGUAGE MultiParamTypeClasses, TypeFamilies #-}
module BodyInstances (B.Vector(..)) where
import Control.Monad (liftM)
import Data.Array.Repa.Eval
import Data.Vector.Unboxed
import qualified Data.Vector.Generic.Mutable as M
import qualified Data.Vector.Generic as G
import qualified Body as B

{-# INLINE uncurry3 #-}
uncurry3 f = \(x,y,z) -> f x y z


newtype instance MVector s (B.Vector) = MV_Vector3 (MVector s (Double,Double,Double))
newtype instance Vector    (B.Vector) = V_Vector3  (Vector    (Double,Double,Double))

instance Unbox (B.Vector)

instance M.MVector MVector (B.Vector) where
  {-# INLINE basicLength #-}
  {-# INLINE basicUnsafeSlice #-}
  {-# INLINE basicOverlaps #-}
  {-# INLINE basicUnsafeNew #-}
  {-# INLINE basicUnsafeReplicate #-}
  {-# INLINE basicUnsafeRead #-}
  {-# INLINE basicUnsafeWrite #-}
  {-# INLINE basicClear #-}
  {-# INLINE basicSet #-}
  {-# INLINE basicUnsafeCopy #-}
  {-# INLINE basicUnsafeGrow #-}
  basicLength (MV_Vector3 v) = M.basicLength v
  basicUnsafeSlice i n (MV_Vector3 v) = MV_Vector3 $ M.basicUnsafeSlice i n v
  basicOverlaps (MV_Vector3 v1) (MV_Vector3 v2) = M.basicOverlaps v1 v2
  basicUnsafeNew n = MV_Vector3 `liftM` M.basicUnsafeNew n
  basicUnsafeReplicate n (B.Vector x y z) = MV_Vector3 `liftM` M.basicUnsafeReplicate n (x,y,z)
  basicUnsafeRead (MV_Vector3 v) i = uncurry3 B.Vector `liftM` M.basicUnsafeRead v i
  basicUnsafeWrite (MV_Vector3 v) i (B.Vector x y z) = M.basicUnsafeWrite v i (x,y,z)
  basicClear (MV_Vector3 v) = M.basicClear v
  basicSet (MV_Vector3 v) (B.Vector x y z) = M.basicSet v (x,y,z)
  basicUnsafeCopy (MV_Vector3 v1) (MV_Vector3 v2) = M.basicUnsafeCopy v1 v2
  basicUnsafeGrow (MV_Vector3 v) n = MV_Vector3 `liftM` M.basicUnsafeGrow v n

instance G.Vector Vector (B.Vector) where
  {-# INLINE basicUnsafeFreeze #-}
  {-# INLINE basicUnsafeThaw #-}
  {-# INLINE basicLength #-}
  {-# INLINE basicUnsafeSlice #-}
  {-# INLINE basicUnsafeIndexM #-}
  {-# INLINE elemseq #-}
  basicUnsafeFreeze (MV_Vector3 v) = V_Vector3 `liftM` G.basicUnsafeFreeze v
  basicUnsafeThaw (V_Vector3 v) = MV_Vector3 `liftM` G.basicUnsafeThaw v
  basicLength (V_Vector3 v) = G.basicLength v
  basicUnsafeSlice i n (V_Vector3 v) = V_Vector3 $ G.basicUnsafeSlice i n v
  basicUnsafeIndexM (V_Vector3 v) i
                = uncurry3 B.Vector `liftM` G.basicUnsafeIndexM v i
  basicUnsafeCopy (MV_Vector3 mv) (V_Vector3 v)
                = G.basicUnsafeCopy mv v
  elemseq _ (B.Vector x y z) w = G.elemseq (undefined :: Vector Double) x
                       $ G.elemseq (undefined :: Vector Double) y
                       $ G.elemseq (undefined :: Vector Double) z w

newtype instance MVector s (B.Body) = MV_Body (MVector s (B.Vector, B.Vector, Double))
newtype instance Vector    (B.Body) = V_Body  (Vector   (B.Vector, B.Vector, Double))

instance Unbox (B.Body)

instance M.MVector MVector (B.Body) where
  {-# INLINE basicLength #-}
  {-# INLINE basicUnsafeSlice #-}
  {-# INLINE basicOverlaps #-}
  {-# INLINE basicUnsafeNew #-}
  {-# INLINE basicUnsafeReplicate #-}
  {-# INLINE basicUnsafeRead #-}
  {-# INLINE basicUnsafeWrite #-}
  {-# INLINE basicClear #-}
  {-# INLINE basicSet #-}
  {-# INLINE basicUnsafeCopy #-}
  {-# INLINE basicUnsafeGrow #-}
  basicLength (MV_Body v) = M.basicLength v
  basicUnsafeSlice i n (MV_Body v) = MV_Body $ M.basicUnsafeSlice i n v
  basicOverlaps (MV_Body v1) (MV_Body v2) = M.basicOverlaps v1 v2
  basicUnsafeNew n = MV_Body `liftM` M.basicUnsafeNew n
  basicUnsafeReplicate n (B.Body x y z) = MV_Body `liftM` M.basicUnsafeReplicate n (x,y,z)
  basicUnsafeRead (MV_Body v) i = uncurry3 B.Body `liftM` M.basicUnsafeRead v i
  basicUnsafeWrite (MV_Body v) i (B.Body x y z) = M.basicUnsafeWrite v i (x,y,z)
  basicClear (MV_Body v) = M.basicClear v
  basicSet (MV_Body v) (B.Body x y z) = M.basicSet v (x,y,z)
  basicUnsafeCopy (MV_Body v1) (MV_Body v2) = M.basicUnsafeCopy v1 v2
  basicUnsafeGrow (MV_Body v) n = MV_Body `liftM` M.basicUnsafeGrow v n

instance G.Vector Vector (B.Body) where
  {-# INLINE basicUnsafeFreeze #-}
  {-# INLINE basicUnsafeThaw #-}
  {-# INLINE basicLength #-}
  {-# INLINE basicUnsafeSlice #-}
  {-# INLINE basicUnsafeIndexM #-}
  {-# INLINE elemseq #-}
  basicUnsafeFreeze (MV_Body v) = V_Body `liftM` G.basicUnsafeFreeze v
  basicUnsafeThaw (V_Body v) = MV_Body `liftM` G.basicUnsafeThaw v
  basicLength (V_Body v) = G.basicLength v
  basicUnsafeSlice i n (V_Body v) = V_Body $ G.basicUnsafeSlice i n v
  basicUnsafeIndexM (V_Body v) i
                = uncurry3 B.Body `liftM` G.basicUnsafeIndexM v i
  basicUnsafeCopy (MV_Body mv) (V_Body v)
                = G.basicUnsafeCopy mv v
  elemseq _ (B.Body x y z) w = G.elemseq (undefined :: Vector B.Vector) x
                       $ G.elemseq (undefined :: Vector B.Vector) y
                       $ G.elemseq (undefined :: Vector Double) z w

instance Elt B.Vector where
    touch (B.Vector x y z) = do
        touch x
        touch y
        touch z

    zero = 0
    one = 1

instance Elt B.Body where
    touch (B.Body p v m) = do
        touch p
        touch v
        touch m

    zero = B.Body 0 0 0
    one = B.Body 1 1 1

