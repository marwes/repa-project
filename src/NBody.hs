{-# LANGUAGE BangPatterns, FlexibleContexts #-}
module NBody where
import Body
import BodyInstances ()
import System.Environment
import ParallelNBody
import SequentialNBody
import qualified Data.Array.Repa as R
import Data.Array.Repa (Z(..), (:.)(..), U)
import System.Random
import Control.Monad (liftM)
import Control.Monad.Identity (runIdentity)
import Control.DeepSeq

dt = 0.1


loop :: (Num a, Eq a) => a -> (b -> b) -> b -> b
loop 0 _ !x = x
loop n f (!x) = loop (n-1) f (f x) 

loopDeep :: (Num a, Eq a, NFData b) => a -> (b -> b) -> b -> b
loopDeep n f !x = go n x
    where
        go 0 !x = x
        go n x = go (n-1) (force $ f x)

loopM :: (Num a, Eq a, Monad m) => a -> (b -> m b) -> b -> m b
loopM 0 _ x = return x
loopM n f x = f x >>= loopM (n-1) f

calculateNBodyS :: Int -> [Body] -> [Body]
calculateNBodyS iterations = loopDeep iterations (advance dt)

fromBodyList :: [Body] -> R.Array R.U R.DIM1 Body
fromBodyList xs = R.fromListUnboxed (Z :. length xs) xs

calculateNBodySR :: Int -> [Body] -> [Body]
calculateNBodySR iterations bs = R.toList
    $ loop iterations (runIdentity . advanceP dt) (fromBodyList bs)

calcP :: (Monad m) => Int -> R.Array R.U R.DIM1 Body -> m (R.Array R.U R.DIM1 Body)
calcP n bs = loopM n (advanceP dt) bs

calcP2 :: Monad m => Int -> Bodies U -> m (Bodies U)
calcP2 !n !bs = loopM n f bs
    where
        f x = advanceP2 dt x >>= R.computeUnboxedP

calcP3 :: Monad m => Int -> Int -> Bodies U -> m (Bodies U)
calcP3 !chunkSize !n !bs = loopM n f bs
    where
        f x = advanceP3 chunkSize dt x >>= R.computeUnboxedP
    


listFromBodyArray :: R.Source r BodyP => Bodies r -> [Body]
listFromBodyArray = Prelude.map fromBodyP . R.toList

arrayFromBodyList :: [Body] -> Bodies U
arrayFromBodyList bs = R.fromListUnboxed shape (map toBodyP bs)
    where
        shape = Z :. (length bs)

action :: String -> Int -> Int -> [Body] -> IO [Body]
action version iterations chunkSize bs =
    case version of
        "-p" -> liftM R.toList $ calcP iterations $ fromBodyList bs
        "-p2" -> liftM listFromBodyArray $ calcP2 iterations $ arrayFromBodyList bs
        "-p3" -> liftM listFromBodyArray $ calcP3 chunkSize iterations $ arrayFromBodyList bs
        "-s" ->  return $ calculateNBodyS iterations bs -- "-s" flag
        "-sr" -> return $ calculateNBodySR iterations bs
        _  -> fail "Expected the -p[N], -s or -sr flag as the first argument"


test = R.fromFunction (Z :. (3 :: Int)) (\_ -> R.fromListUnboxed (Z :. (3 :: Int)) [1 :: Double,2,3])

mainNBody args = do
    let
        version:iterations:rest = args
        numBodies = if null rest then 2000 else read $ head rest
        chunkSize = read $ if length rest >= 2 then rest !! 1 else "100"
        numIterations = read iterations
        gen = mkStdGen numBodies
        bs = take numBodies $ randoms gen :: [Body]
    putStrLn $ "Number of Bodies: " ++ show numBodies
    putStrLn $ "Iterations: " ++ show numIterations
    putStrLn $ "Chunksize: " ++ show chunkSize
    result <- action version numIterations chunkSize bs 
    putStrLn $ show $ (result `deepseq` head result)


testParallell :: (R.Source r Double) => R.Array r R.DIM1 Double -> IO Double
testParallell = R.sumAllP . R.map (+1) 

