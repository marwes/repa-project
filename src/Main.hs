{-# LANGUAGE BangPatterns, FlexibleContexts #-}
module Main where
import System.Environment (getArgs)
import NBody

--Run the program wth one argument which is the number of iterations to run the NBody algorithm for
main = getArgs >>= mainNBody
