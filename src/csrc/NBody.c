/* The Computer Language Benchmarks Game
 * http://benchmarksgame.alioth.debian.org/
 *
 * contributed by Christoph Bauer
 *  
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define pi 3.141592653589793
#define solar_mass (4 * pi * pi)
#define days_per_year 365.24

struct planet {
  double x, y, z;
  double vx, vy, vz;
  double mass;
};

void advance(int nbodies, struct planet * bodies, double dt)
{
  int i, j;

  for (i = 0; i < nbodies; i++) {
    struct planet * b = &(bodies[i]);
    for (j = i + 1; j < nbodies; j++) {
      struct planet * b2 = &(bodies[j]);
      double dx = b->x - b2->x;
      double dy = b->y - b2->y;
      double dz = b->z - b2->z;
      double distance = sqrt(dx * dx + dy * dy + dz * dz);
      double mag = dt / (distance * distance * distance);
      b->vx -= dx * b2->mass * mag;
      b->vy -= dy * b2->mass * mag;
      b->vz -= dz * b2->mass * mag;
      b2->vx += dx * b->mass * mag;
      b2->vy += dy * b->mass * mag;
      b2->vz += dz * b->mass * mag;
    }
  }
  for (i = 0; i < nbodies; i++) {
    struct planet * b = &(bodies[i]);
    b->x += dt * b->vx;
    b->y += dt * b->vy;
    b->z += dt * b->vz;
  }
}

double energy(int nbodies, struct planet * bodies)
{
  double e;
  int i, j;

  e = 0.0;
  for (i = 0; i < nbodies; i++) {
    struct planet * b = &(bodies[i]);
    e += 0.5 * b->mass * (b->vx * b->vx + b->vy * b->vy + b->vz * b->vz);
    for (j = i + 1; j < nbodies; j++) {
      struct planet * b2 = &(bodies[j]);
      double dx = b->x - b2->x;
      double dy = b->y - b2->y;
      double dz = b->z - b2->z;
      double distance = sqrt(dx * dx + dy * dy + dz * dz);
      e -= (b->mass * b2->mass) / distance;
    }
  }
  return e;
}

void offset_momentum(int nbodies, struct planet * bodies)
{
  double px = 0.0, py = 0.0, pz = 0.0;
  int i;
  for (i = 0; i < nbodies; i++) {
    px += bodies[i].vx * bodies[i].mass;
    py += bodies[i].vy * bodies[i].mass;
    pz += bodies[i].vz * bodies[i].mass;
  }
  bodies[0].vx = - px / solar_mass;
  bodies[0].vy = - py / solar_mass;
  bodies[0].vz = - pz / solar_mass;
}

void initPlanet(struct planet* p)
{
    p->mass = abs(rand());
    p->x = ((double)rand()) / RAND_MAX;
    p->y = ((double)rand()) / RAND_MAX;
    p->z = ((double)rand()) / RAND_MAX;
    p->vx = ((double)rand()) / RAND_MAX;
    p->vy = ((double)rand()) / RAND_MAX;
    p->vz = ((double)rand()) / RAND_MAX;
}

struct planet* newPlanets(size_t numBodies)
{
    int i = 0;
    struct planet* bodies = malloc(numBodies * sizeof (struct planet));
    for (i = 0; i < numBodies; ++i)
    {
        initPlanet(bodies + i);
    }
    return bodies;
}

void advanceTimes(int n, double dt, struct planet* planets, size_t numPlanets)
{
    int i;
    for (i = 1; i <= n; i++)
        advance(numPlanets, planets, dt);
}


int c_main(int argc, char** argv)
{
    if (argc < 3)
    {
        printf("Expected atleast 2 arguments, iterations numBodies");
        return 1;
    }
    int n = atoi(argv[1]);
    int numBodies = atoi(argv[2]);
    printf("Running %d iterations with %d bodies\n", n, numBodies);
    struct planet* bodies = newPlanets(numBodies);

    offset_momentum(numBodies, bodies);
    printf ("%.9f\n", energy(numBodies, bodies));
    advanceTimes(n, 0.01, bodies, numBodies);
    printf ("%.9f\n", energy(numBodies, bodies));
    return 0;
}

