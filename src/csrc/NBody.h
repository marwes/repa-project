#ifndef __NBODY_H__
#define __NBODY_H__

#include <stddef.h>

struct planet;

struct planet* newPlanets(size_t numBodies);

#endif __NBODY_H__
