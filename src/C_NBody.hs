{-# LANGUAGE ForeignFunctionInterface #-}
module C_NBody (
    PlanetArray,
    newPlanets,
    advanceTimes
    ) where
import Foreign
import Foreign.C.Types

newtype PlanetPtr = PlanetPtr (Ptr PlanetPtr)

foreign import ccall "newPlanets" c_newPlanets :: CSize -> IO (PlanetPtr)
foreign import ccall "advanceTimes" c_advanceTimes :: CInt -> CDouble -> PlanetPtr -> CSize -> IO ()

data PlanetArray = PlanetArray PlanetPtr CSize

newPlanets :: CSize -> IO PlanetArray
newPlanets size = do
    ptr <- c_newPlanets size
    return $ PlanetArray ptr size

advanceTimes :: CInt -> CDouble -> PlanetArray -> IO ()
advanceTimes n dt (PlanetArray ptr size) = c_advanceTimes n dt ptr size
