{-# LANGUAGE BangPatterns #-}
module SequentialNBody (
    Body(..),
    Vector(..),

    advance,
    advancePosition
) where
import Data.Foldable (foldl')
import Body

advance :: Double -> [Body] -> [Body]
advance dt bs = map (advancePosition dt . advanceVelocity dt bs) bs

{-# INLINE advancePosition #-}
advancePosition :: Double -> Body -> Body
advancePosition dt (Body pos vel mass) = Body (pos + vel * Vector dt dt dt) vel mass

advanceVelocity :: Double -> [Body] -> Body -> Body
advanceVelocity dt bs body@(Body pos vel mass) = Body pos (vel + accel dt body bs) mass
	
accel :: Double -> Body -> [Body] -> Vector
accel dt (Body pos _ _) bs = foldl' addAcceleration 0 bs
	where
		addAcceleration accel (Body otherPos _ otherMass) = accel + calcAcceleration dt pos otherPos otherMass
