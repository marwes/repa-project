module Tests where
import Test.QuickCheck
import Test.QuickCheck.Monadic
import Control.Applicative
import Control.Monad
import Body
import SequentialNBody
import NBody


main = mapM_ (\(name, test) -> putStrLn name >> test) tests

tests = [
    ("calcAcceleration_equal_pos", quickCheck prop_calcAcceleration_equal_pos),
    ("force_nomass", quickCheck prop_force_nomass),
    ("nomass_update", quickCheck prop_nomass_update),
    ("nbody_equal", quickCheck prop_nbody_equal),
    ("nbody_par_equal", quickCheck prop_nbody_par_equal)
    ]

instance Arbitrary (Vector) where
	arbitrary = Vector <$> arbitrary <*> arbitrary <*> arbitrary

instance Arbitrary Body where
	arbitrary = Body <$> arbitrary <*> arbitrary <*> arbitrary

prop_calcAcceleration_equal_pos pos = calcAcceleration 0.1 pos pos 30 == Vector 0 0 0


prop_force_nomass :: Double -> Body -> Body -> Bool
prop_force_nomass dt (Body pos1 _ _) (Body pos2 _ _) = calcAcceleration dt pos1 pos2 0 == Vector 0 0 0

prop_nomass_update :: Double -> [Body] -> Property
prop_nomass_update dt bs = (length bs >= 2) ==> map getPosition noMassUpdate == (map getPosition massUpdate)
	where
		removeMass (Body p v _) = Body p v 0
		massLess = (map removeMass bs)
		noMassUpdate = advance dt massLess
		massUpdate = map (advancePosition dt) bs

prop_nbody_par_equal :: [Body] -> Int -> Property 
prop_nbody_par_equal bs n = monadicIO $ do
    l <- calcP i $ arrayFromBodyList bodies
    r <- run $ calcP2 i $ arrayFromBodyList bodies
    assert (listFromBodyArray l ~= listFromBodyArray r)
    return ()
    where
        i = abs n `mod` 10
        bodies = take 25 bs

--test that the aprallell implementation gives the same result as the sequential algorithm
prop_nbody_equal :: [Body] -> Int -> Property
prop_nbody_equal bs n = monadicIO $ do
    r <- liftM listFromBodyArray $ calcP i $ arrayFromBodyList bodies
    assert (calculateNBodyS i bodies ~= r)
    where
        i = abs n `mod` 50
        bodies = take 50 bs
