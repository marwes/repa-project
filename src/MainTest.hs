{-# LANGUAGE BangPatterns #-}
module Main (main) where
import Control.Monad.Identity (runIdentity)
import System.Random
import GHC.Conc.Sync (getNumCapabilities)
import qualified Criterion as C
import qualified Criterion.Main as C
import qualified Data.Array.Repa as R
import NBody
import ParallelNBody (BodyP)
import Body (Body)
import C_NBody


benchN f bs = C.bench (show n) $ C.nf f bs
    where
        !n = length bs

benchNRepa f bs = C.bench (show n) $ C.whnf f bs
    where
        (R.Z R.:. n) = R.extent bs

bodyArray :: Int -> [Body] -> R.Array R.U R.DIM1 Body
bodyArray n bs = R.deepSeqArray x x
    where
        !x = R.fromListUnboxed (R.ix1 n) (take n bs)

bodyPArray :: Int -> [Body] -> R.Array R.U R.DIM1 BodyP
bodyPArray n bs = r
    where
        !r = R.deepSeqArray x x
        !x = arrayFromBodyList (take n bs)


main = do
    gen <- newStdGen
    capabilities <- getNumCapabilities
    cPlanets1 <- newPlanets 10000
    cPlanets4 <- newPlanets 4000

    let bs = randoms gen
    !arrP1 <- return $ bodyPArray 10000 bs
    !arrP4 <- return $ bodyPArray 4000 bs
    
    !arr1 <- return $ bodyArray 10000 bs
    !arr4 <- return $ bodyArray 4000 bs

    let iterations :: Num a => a
        iterations = 4
        sequential = calculateNBodyS iterations
        parallel1 = benchNRepa (runIdentity . calcP iterations)
        parallel3 = benchNRepa (runIdentity . calcP3 100 iterations)
        sequentialTests =  C.bgroup "Sequential" [
                benchN sequential (take 10000 bs),
                benchN sequential (take 4000 bs)
            ]
        parallelTests = [
            C.bgroup "Parallel1" [
                parallel1 arr1,
                parallel1 arr4
            ]
            ]
        cTests = [
            C.bgroup "C" [
                    C.bench "10000" $ C.whnfIO (advanceTimes iterations 0.1 cPlanets1),
                    C.bench "4000" $ C.whnfIO (advanceTimes iterations 0.1 cPlanets4)
                ]]

        tests = if capabilities == 1
                then sequentialTests : cTests ++ parallelTests
                else parallelTests
    C.defaultMain tests
        

