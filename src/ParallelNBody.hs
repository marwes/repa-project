{-# LANGUAGE FlexibleContexts, BangPatterns #-}
module ParallelNBody (
    Bodies,
    BodyP,

    advanceP,
    advanceP2,
    advanceP3,

    fromBodyP,
    toBodyP
) where
import Data.Array.Repa as R
import Body
import Control.Monad (liftM, forM_, forM)
import Control.Monad.Identity (runIdentity)
import Data.Foldable (foldl')
import Data.Vector.Unboxed (Unbox)
import Control.DeepSeq
import SequentialNBody
import BodyInstances

type VectorP = (Double, Double, Double)
type Position = VectorP
type Velocity = VectorP
type BodyP = (Position, Velocity, Double)
type Bodies r = Array r DIM1 BodyP


advanceP :: Monad m => Double -> Array U DIM1 Body -> m (Array U DIM1 Body)
advanceP !dt !bs = R.computeUnboxedP $ R.map (advancePosition dt . advanceVelocityP dt bs) bs

advancePositionP :: Double -> BodyP -> BodyP
advancePositionP !dt (!pos, !vel, !mass) = (newPos, vel, mass)
	where
        newPos = pos .+ (vel .* (dt, dt, dt))

advanceVelocityP :: Double -> Array U DIM1 Body -> Body -> Body
advanceVelocityP !dt !bs !body@(Body pos vel mass) = Body pos newVel mass
    where
        newVel = accelP dt body bs + vel

accelP :: Double -> Body -> Array U DIM1 Body -> Vector
accelP !dt (Body pos _ _) !bs = acc
	where
        Body acc _ _ = R.foldAllS (addAcceleration dt pos) (Body 0 0 0) bs
        
addAcceleration :: Double -> Vector -> Body -> Body -> Body
addAcceleration !dt !pos (Body otherPos _ otherMass) (Body ax vel m) = (Body newAcc vel m)
    where
        !newAcc = ax + calcAcceleration dt pos otherPos otherMass

advanceP2 :: (Monad m) => Double -> Bodies U -> m (Bodies D)
advanceP2 dt bs = do
    !a <- accelerations bs
    return $! R.zipWith (addAcc dt) bs a

{-# INLINE addAcc #-}
addAcc !dt (!pos, !vel, !mass) !acc =
    let
        !newvel = vel .+ acc
        !newPos = pos .+ (newvel .* (dt, dt, dt))
    in
        (newPos, newvel, mass)


accelerations :: (Monad m, Source r BodyP) => Array r DIM1 BodyP -> m (Array U DIM1 (Double, Double, Double))
accelerations bs = foldP (.+) (0,0,0) $ vectorMultiply bs (forceBetween)

{-# INLINE forceBetween #-}
forceBetween :: BodyP -> BodyP -> (Double, Double, Double)
forceBetween !(!pos1, _, _) !(!pos2, _, !mass2) = calcAccelerationP 0.1 pos1 pos2 mass2

advanceP3 :: Monad m => Int -> Double -> Bodies U -> m (Bodies D)
advanceP3 !chunkSize !dt !bs = do
    let acc = calcSlice chunkSize bs
    return $! R.zipWith (addAcc dt) bs acc

calcSlice :: Int
    -> Array U DIM1 BodyP
    -> Array U DIM1 VectorP
calcSlice chunkSize bs = runIdentity $ do
    let Z :. size = R.extent bs
        chunks = (size `div` chunkSize) - 1
        range = R.fromFunction (Z :. size) (\(Z :. i) -> i)
        computationLine y = (flip Prelude.map) [0..chunks] (\x ->
                let c =  chunk chunkSize bs forceBetween x y in runIdentity $ R.foldP (.+) (0,0,0) c
            ) 
        summed y =
            let
                line = computationLine y
                zeroes = R.computeUnboxedS $ R.fromFunction (Z :. size) (\_ -> (0,0,0))
                result :: Array D DIM1 VectorP
                result = foldl' (\x y -> R.zipWith (.+) x y) (delay zeroes) line
            in  result
        listOfComputes = Prelude.map summed [0..chunks]
    
    return $ R.computeUnboxedS $ foldl (R.append) (delay $ head listOfComputes) $ tail listOfComputes


{-# INLINE chunk #-}
chunk :: (Unbox b, Source r a) => Int -> Array r DIM1 a -> (a -> a -> b) -> Int -> Int -> Array D DIM2 b
chunk chunkSize bs f x y = R.traverse2 sliceI sliceJ transform worker
    where 
        transform (Z :. i) (Z :. j) = Z :. i :. j
        worker fi fj (Z :. i :. j) = f (fi (Z :. i)) (fj (Z :. j))
        
        sliceI = extract (Z :. x) (Z :. chunkSize) bs
        sliceJ = extract (Z :. y) (Z :. chunkSize) bs

vectorMultiply ::
    (Source r2 b) =>
    Array r2 DIM1 b ->
    (b -> b -> c) -> Array D DIM2 c 
vectorMultiply bs worker = R.traverse2 bs bs shapeTransform f
    where
        {-# INLINE shapeTransform #-}
        shapeTransform (Z :. i) (Z :. j) = Z :. i :. j
        {-# INLINE f #-}
        f l r (Z :. i :. j) = worker (l (Z :. i)) (r (Z :. j))


epsilon = 0.0001
{-# INLINE calcAccelerationP #-}
calcAccelerationP :: Double -> (Double, Double, Double) -> (Double, Double, Double) -> Double -> (Double, Double, Double)
calcAccelerationP !dt !pos1 !pos2 !mass = result
	where
        result@(!_,!_,!_) = delta .* (magnitude, magnitude, magnitude)
        delta@(dx, dy, dz) = pos2 .- pos1
        !squaredDistance = dx * dx + dy * dy + dz * dz
        !magnitude = dt * mass / ((squaredDistance * sqrt squaredDistance) + epsilon)

infixl 7 .*
infixl 6 .+


{-# INLINE (.+) #-}
(!x1,!y1,!z1) .+ (!x2,!y2,!z2) = (x1 + x2, y1 + y2, z1 + z2)
{-# INLINE (.-) #-}
(!x1,!y1,!z1) .- (!x2,!y2,!z2) = (x1 - x2, y1 - y2, z1 - z2)
{-# INLINE (.*) #-}
(!x1,!y1,!z1) .* (!x2,!y2,!z2) = (x1 * x2, y1 * y2, z1 * z2)

toBodyP (Body pos vel mass) = (toTuple pos, toTuple vel, mass)
fromBodyP (pos, vel, mass) = Body (fromTuple pos) (fromTuple vel) mass
